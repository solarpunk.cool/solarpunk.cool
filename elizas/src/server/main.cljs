(ns server.main
  (:require
   [cljs.core.async :refer [<!]] [cljs-http.client :as http])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(def value-a 1)
(def value-b 2)
value-a

(defn reload!
  []
  (println "Code updated.")
  (println "Trying values: " value-a value-b))

(defn main!
  []
  (println "App loaded!"))


(go (let [response (<! (http/get "https://github.com"))]
      (prn (:status response))
      (prn (:body response))))
