◊define-meta[title]{Introduction}

Welcome to the wonderful world of CSS!  The path you are taking will lead you to an incredible place where, with just a few well-placed curly braces, your aesthetic dreams are realized on the screen.
...except for that one dream that won't budge from the top left and is bright green for some reason, and so you run through all your well-placed curly braces and none will tell you why this is happening and soon, soon you'll be in a world where you are yelling at your screen, yelling at all your dreams, telling them to fix themselves.
...Until suddenly you discover the semi-colon you forgot to add, or a CSS rule whose nuance you misunderstood, and you'll fix this property and see everything fall into the exact colour and shape you asked for...and oh that ecstasy you will feel; it will feel like another world.

Learning CSS is a beautiful thing, whose benefits far outstrip its frustrations.  There ◊em{will be frustrations}, and CSS problems have a unique way of getting under your skin.  There's no point denying this.  It's with the same honesty that I'll say the problems are worth it, they'll introduce you to a new level of understanding, and when you reach a comfortable place with your CSS, it will feel truly like your own aesthetic from the code up to the screen and that feeling is chef's kiss.

This zine is an introduction to the language, its relationship to HTML, and some patterns you can practice to develop your own online aesthetic.  I am a CSS enthusiast, but am in no way an expert, and there are many different ways one can write and structure CSS.  So at the end of the zine will be some good resources to get a more expert take on CSS, this is just the hyped up beginning.
