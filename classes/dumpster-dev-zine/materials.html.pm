◊define-meta[title]{Materials}


To Make a webpage you will need...

◊ul{
◊li{A text editor that can write plain text}
◊li{A web browser}
◊li{heart}
}

For text editor, the simplest ones to get started with are:
◊ul{
◊li{Mac: TextEdit (though make sure plaintext is enabled by going to ◊code{format -> make PlainText)}
◊li{Windows: Notepad}
◊li{Linux: Leaf}
}

For a Browser we recommend (ranked):
◊ol{
◊li{Firefox}
◊li{Beaker}
◊li{Chrome}
◊li{Safari}}



