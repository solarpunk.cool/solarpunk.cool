#lang quadwriter/markup

#:page-orientation "wide"
#:font-size 16
#:footer-display false

◊h1{Materials Needed to make a webpage}
◊p{}
◊p{
   ◊ul{
       ◊li{text editor that works with plaintext}
       ◊li{web browser}
       ◊li{heart}}}

◊h1{Text Editor Recommendations}
◊em{your text editor will become your most important and cherished tool.  Here's some good ones to get started.}
◊p{}
    ◊strong{For Mac: TextEdit}
               (Make sure it's set to plain text, by clicking ◊code{Format>Make PlaintText})}
    ◊p{}
    ◊strong{For Windows: Notepad}
◊p{}
    ◊strong{For Linux: Leaf}

◊h1{<p>}


◊em{short for 'paragraph', the ◊code{<p>} tag is used to separate blocks of text in your website.}


◊h2{}

◊code{
      <p>I thought of many cool things throughout that afternoon.</p>
      <p>The next morning, I walked to the bus.</p>}

◊em{becomes...}

I thought of many cool thrings throughout that afternoon.


The next morning, I walked to the bus.


◊h1{<strong> and <em>}

◊em{Ways to emphasize sections of your text.  By default, ◊code{<strong>} bolds and ◊code{<em>} italicizes your text.}


◊h2{}

◊code{
      <p>I thought of <em>many</em> cool things throughout that <strong>afternoon</strong>.</p>
      }

◊em{becomes...}



I thought of ◊em{many} cool thrings throughout that ◊strong{afternoon}.



