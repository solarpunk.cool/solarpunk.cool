◊define-meta[title]{The <p> Tag}
◊define-meta[tag]{p}

    ◊em{Short for paragraph, the <p> tag marks the separate sections of your text}

    ◊example[#:code ""]{}
