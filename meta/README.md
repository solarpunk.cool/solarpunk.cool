# Solarpunk.cool

It's a site it's a feeling it's an aesthetic it's a place it's tools and patterns it's us!

We go into more detail with the various files you'll find here.

**This is a big ol work in progress**.  If you are reading this it's likely you just stumbled upon it, and we didn't intentionally share it.  That's okay!  But please keep that in mind if you are looking for clarity and not finding it here.  We are looking for clarity too, which is why we started up this repo!  At this moment it is a rocky patch of earth that we are starting to dig into, trying to make it a black soil.
